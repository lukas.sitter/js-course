let currentResult = 0;
const logEntries = [];
let allEntries;

const getUserNumberInput = () => parseFloat(userInput.value);

const createAndWriteOutput = (operator, resultBeforeCalc, calcNumber) => {
  const calcDescription = `${resultBeforeCalc} ${operator} ${calcNumber}`;
  outputResult(currentResult, calcDescription, allEntries);
};

const writeToLog = (operationIdentifier, newPrevResult, enteredNumber, newResult) => {
  const logEntry = {
    operation: operationIdentifier,
    prevResult: newPrevResult,
    number: enteredNumber,
    result: newResult,
  };
  logEntries.push(logEntry);
  console.log(logEntries);
};

const createLogOfAllEntries = () => {
  allEntries = logEntries.map((item) => item.result);
  const entries = allEntries.toString();
  console.log(entries);
};

const calculate = (operation) => {
  const enteredNumber = getUserNumberInput();
  if (!enteredNumber) {
    return;
  }
  const initialResult = currentResult;
  let mathOperator;
  if (operation === 'ADD') {
    currentResult += enteredNumber;
    mathOperator = '+';
  } else if (operation === 'SUBTRACT') {
    currentResult -= enteredNumber;
    mathOperator = '-';
  } else if (operation === 'MULTIPLY') {
    currentResult *= enteredNumber;
    mathOperator = '*';
  } else if (operation === 'DIVIDE') {
    currentResult /= enteredNumber;
    mathOperator = '/';
  }
  writeToLog(operation, initialResult, enteredNumber, currentResult);
  createLogOfAllEntries();
  createAndWriteOutput(mathOperator, initialResult, enteredNumber);
};

addBtn.addEventListener('click', calculate.bind(this, 'ADD'));
subtractBtn.addEventListener('click', calculate.bind(this, 'SUBTRACT'));
multiplyBtn.addEventListener('click', calculate.bind(this, 'MULTIPLY'));
divideBtn.addEventListener('click', calculate.bind(this, 'DIVIDE'));
